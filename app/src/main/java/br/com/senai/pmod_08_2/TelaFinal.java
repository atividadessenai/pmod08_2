package br.com.senai.pmod_08_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class TelaFinal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_final);

        Intent intent = getIntent();
        ArrayList<String> dadosInformados = intent.getStringArrayListExtra("dados_informados");

        int i = 0;
        for (String item : dadosInformados) {

            i++;
        }

        ListView resultado = (ListView)findViewById(R.id.lvResultado);

        ArrayAdapter<String> resultadoAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                dadosInformados
        );
        resultado.setAdapter(resultadoAdapter);
        resultado.setEnabled(false);
    }
}
