package br.com.senai.pmod_08_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.senai.pmod_08_2.interfaces.ActivityInterface;

public class TelaPrincipal extends AppCompatActivity implements ActivityInterface, View.OnClickListener {
    private final static String TAG = "TELA_PRINCIPAL";

    ArrayList<String> intentExtra = new ArrayList<>();

    EditText etNome;
    EditText etTelefone;

    RadioGroup rdGroupGenero;
    RadioGroup rdGroupTipoTelefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        // EditTexts
        etNome = (EditText)findViewById(R.id.etNome);
        etTelefone = (EditText)findViewById(R.id.etTelefone);

        // RadioGroups
        rdGroupGenero = (RadioGroup)findViewById(R.id.rdGroupGenero);
        rdGroupTipoTelefone = (RadioGroup)findViewById(R.id.rdGroupTipoTelefone);

        // Button - Enviar
        Button btEnviar = (Button)findViewById(R.id.btEnviar);
        btEnviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            // Validaçao dos inputs
            validarEntrada();

            // Carregamento da activity
            alterarIntent();
        } catch (Exception e) {
            Log.w(TAG, e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void alterarIntent() {
        Intent telaSecundariaIntent = new Intent(this, TelaSecundaria.class);

        telaSecundariaIntent = adicionarIntentExtra(telaSecundariaIntent);

        startActivity(telaSecundariaIntent);
    }

    public Intent adicionarIntentExtra(Intent intent) {

        RadioButton genero = (RadioButton)findViewById(rdGroupGenero.getCheckedRadioButtonId());
        RadioButton tipo_telefone = (RadioButton)findViewById(rdGroupTipoTelefone.getCheckedRadioButtonId());

        intentExtra.add(etNome.getText().toString());
        intentExtra.add(genero.getText().toString());
        intentExtra.add(etTelefone.getText().toString());
        intentExtra.add(tipo_telefone.getText().toString());

        intent.putStringArrayListExtra("dados_informados", intentExtra);

        return intent;
    }

    @Override
    public void validarEntrada() throws Exception {
        String mensagem = "";
        if (etNome.getText().toString().trim().isEmpty()) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.nome)) + "\n";
        }
        if (rdGroupGenero.getCheckedRadioButtonId() == -1) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.genero)) + "\n";
        }
        if (etTelefone.getText().toString().trim().isEmpty()) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.telefone)) + "\n";
        }
        if (rdGroupTipoTelefone.getCheckedRadioButtonId() == -1) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.tipo_telefone)) + "\n";
        }

        if (!mensagem.isEmpty()) {
            throw new Exception(mensagem);
        }
    }
}
