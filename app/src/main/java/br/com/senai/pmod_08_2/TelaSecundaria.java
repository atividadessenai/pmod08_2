package br.com.senai.pmod_08_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.senai.pmod_08_2.interfaces.ActivityInterface;

public class TelaSecundaria extends AppCompatActivity implements ActivityInterface, View.OnClickListener {
    private final static String TAG = "TELA_SECUNDARIA";

    ArrayList<String> intentExtra = new ArrayList<>();

    Spinner spCursos;
    Spinner spLocais;
    CheckBox chManha;
    CheckBox chTarde;
    CheckBox chNoite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_secundaria);

        Intent intent = getIntent();
        intentExtra = intent.getStringArrayListExtra("dados_informados");

        // Spinner - Cursos
        ArrayAdapter<CharSequence> spCursosAdapter = ArrayAdapter.createFromResource(this, R.array.cursos, android.R.layout.simple_spinner_item);
        spCursosAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spCursos = (Spinner)findViewById(R.id.spCursos);
        spCursos.setAdapter(spCursosAdapter);

        // Spinner - Locais
        ArrayAdapter<CharSequence> spLocaisAdapter = ArrayAdapter.createFromResource(this, R.array.locais, android.R.layout.simple_spinner_item);
        spLocaisAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spLocais = (Spinner)findViewById(R.id.spLocal);
        spLocais.setAdapter(spLocaisAdapter);

        // Checkbox
        chManha = (CheckBox)findViewById(R.id.chManha);
        chTarde = (CheckBox)findViewById(R.id.chTarde);
        chNoite = (CheckBox)findViewById(R.id.chNoite);

        // Button - Enviar
        Button btEnviar = (Button)findViewById(R.id.btEnviar);
        btEnviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            // Validaçao das entradas
            validarEntrada();

            // Altera a activity
            alterarIntent();
        } catch (Exception e) {
            Log.w(TAG, e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void alterarIntent() {
        Intent telaFinalIntent = new Intent(this, TelaFinal.class);

        telaFinalIntent = adicionarIntentExtra(telaFinalIntent);

        startActivity(telaFinalIntent);
    }

    @Override
    public void validarEntrada() throws Exception {
        String mensagem = "";

        if (spCursos.getSelectedItemId() == 0) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.curso)) + "\n";
        }

        if (!chManha.isChecked() && !chTarde.isChecked() && !chNoite.isChecked()) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.periodo)) + "\n";
        }

        if (spLocais.getSelectedItemId() == 0) {
            mensagem += String.format(getString(R.string.campo_obrigatorio), getString(R.string.local)) + "\n";
        }

        if (!mensagem.isEmpty()) {
            throw new Exception(mensagem);
        }
    }

    @Override
    public Intent adicionarIntentExtra(Intent intent) {

        intentExtra.add(spCursos.getSelectedItem().toString());
        intentExtra.add(spLocais.getSelectedItem().toString());

        intent.putStringArrayListExtra("dados_informados", intentExtra);

        return intent;
    }
}
