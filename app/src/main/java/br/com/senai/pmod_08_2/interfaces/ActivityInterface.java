package br.com.senai.pmod_08_2.interfaces;

import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by guilherme on 10/02/17.
 */

public interface ActivityInterface {
    ArrayList<String> intentExtra = new ArrayList<String>();

    void alterarIntent();
    void validarEntrada() throws Exception;
    Intent adicionarIntentExtra(Intent intent);
}
